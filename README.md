# Static Baker

## Usage

Place in to res://addons/static_baker/ and toggle plugin to be active.

Requires https://github.com/SaracenOne/mesh_combiner to be also installed.

A new node StaticBakerGroup is added.

Add scenes to StaticBakerGroup and click Pack Meshes.

## Open problems

* Does not work with animated meshes.
* Redo isn't working.
* Materials don't copy
* DONE: Does not equire all child nodes to be instanced scenes
    * TODO The pack button takes the grouped nodes, converts them into instances, writes them into the filesystem, and then packs them.
    * DONE Warn if there are non scene nodes underneath group and stop attempts to pack such trees.