extends Node
tool

# A functional reduction of the array.
#
# Take two slots from the back of the work array, pass them to a method, then take the return value and 
# add it back to the front of the work array.
# Re-call the same method, continuously until you get to the end, at which point the final method 
# is called on the last result.
#
#func _ready():
#	var add = parallel_reduce_const.new()
#	add.connect("notify", self, "notify")
#	add.parallel_reduce_notify({}, [1,2,3], self, "add", self, "handle_add", self, "notify")
#
#func add(a,b):
#	return a + b
#
#func handle_add(state, result):
#	print(result)
#
#func notify(state):
#	var percent = 1 - (float(state["_work"].size()) / float(state["_work_size"]))
#	print(String(percent))
var show_logs = false
const output_buffer = 80
signal notify(state)

func parallel_reduce(p_state : Dictionary, p_work : Array,
		p_reduce_object : Object, p_reduce_function : String, 
		p_finish_object : Object, p_finish_function : String):
	p_state["_reduce"] = {"object": p_reduce_object, "function": p_reduce_function}
	p_state["_work"] = p_work
	p_state["_work_size"] = p_state["_work"].size()
	parallel_for(p_state,
		self, "_reduce",
		p_finish_object, p_finish_function)

func parallel_for(p_state : Dictionary,
		p_transform_object : Object, p_transform_function : String, 
		p_finish_object : Object, p_finish_function : String):
	var thread = Thread.new()
	p_state["_control_main"] = thread
	p_state["_transform"] = {"object": p_transform_object, "function": p_transform_function}
	p_state["_call_finish"] = {"object": p_finish_object, "function": p_finish_function}
	p_state["_control_main"].start(self, "_worker_thread", p_state)

func set_show_logs(show : bool):
	show_logs = show

func _worker_thread(state):
	var threads = []
	var task_count = OS.get_processor_count()
	threads.resize(task_count)
	for i in range(task_count):
		threads[i] = Thread.new()
	state["_control_tasks"] = threads
	state["_mutex"] = Mutex.new()
	if Engine.editor_hint and show_logs:
		var empty_string : String
		var backspace_string : String
		for i in range(output_buffer):
			empty_string = empty_string + " "
			backspace_string = backspace_string + "\b"
		state["_empty_string"] = empty_string
		state["_backspace_string"] = backspace_string
		printraw(state["_empty_string"])
	for i in range(task_count):
		threads[i].start(self, "_single_thread_combine", state)
	var result = {}
	for i in range(task_count):
		result = threads[i].wait_to_finish()
	if Engine.editor_hint and show_logs:
		printraw("\n")
	var work = result["_work"].pop_back()
	result.erase("_work")
	result.erase("_control_tasks")
	result.erase("_reduce")
	result.erase("_mutex")
	result.erase("_empty_string")
	result.erase("_backspace_string")
	var obj = state["_call_finish"].object
	var function = state["_call_finish"].function
	result.erase("_call_finish")
	obj.call(function, result, work)

func _reduce(state):
	while state["_work"].size() > 1:
		emit_signal("notify", state)
		state["_mutex"].lock()
		var l = state["_work"].pop_back()
		var r = state["_work"].pop_back()
		state["_mutex"].unlock()
		if Engine.editor_hint and show_logs:
			var processing = "Processing: " + String(state["_work"].size()) + " remaining of " + String(state["_work_size"])
			var line = processing + state["_empty_string"]
			line = line.substr(0, output_buffer)
			printraw(state["_backspace_string"] + line)
		var result = state["_reduce"].object.call(state["_reduce"].function, l, r)
		state["_mutex"].lock()
		state["_work"].push_front(result)
		state["_mutex"].unlock()
	return state

func _single_thread_combine(state):
	return state["_transform"].object.call(state["_transform"].function, state)