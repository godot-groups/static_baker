tool
extends EditorPlugin

const static_baker_group_const = preload("static_baker_group.gd")
const material_replacer_const = preload("material_replacer.gd")

var editor_interface = null
var button = null
var selected_node = null

func update_button_label():
	if selected_node.get_script() != static_baker_group_const:
		return
	if not button:
		return
	if selected_node.original_instances.size() == 0:
		button.set_text(tr("Pack Meshes"))
		return
	button.set_text(tr("Unpack Meshes"))

func set_button_percent_label(percent : float):
	percent = round(percent * 100)
	button.set_text("Packing: " + String(percent) + "%")

func set_all_owner(node, owner):
	for N in node.get_children():
		if N.get_child_count() > 0:
			N.set_owner(owner)
			set_all_owner(N, owner)
		else:
			N.set_owner(owner)

func save_branch_as_scene_recursive(node, owner):
	for N in node.get_children():
		var path = N.get_tree().edited_scene_root.filename.get_basename() + \
		"-" + N.name + "-" + String(N.get_path()).md5_text() + ".tscn"
		if N.get_child_count() > 0:
			if not can_save_node_as_scene(N, node):
				save_branch_as_scene_recursive(N, owner)
				continue
			set_all_owner(N, N)
			save_node_as_scene(N, path, selected_node.get_owner())
		else:
			if can_save_node_as_scene(N, node):
				save_node_as_scene(N, path, selected_node.get_owner())

func reparent_node(p_target, p_source):
	var target = p_target
	var source = p_source
	self.call_deferred("remove_child", source)
	target.add_child(source)
	source.set_owner(target)

func can_save_node_as_scene(node, parent):
	var is_scene = node.get_filename() != ""
	var is_parent_scene = parent.get_filename() != ""
	var is_child_of_instanced_scene = node.get_owner() != selected_node.get_owner()
	return not is_scene and not is_child_of_instanced_scene and (parent == selected_node or is_parent_scene)

func _process_static_baker_group():
	if(not selected_node):
		return
	if(selected_node.get_script() != static_baker_group_const):
		return
	if not (selected_node.original_instances.size() > 0):
		save_branch_as_scene_recursive(selected_node, selected_node.get_owner())
	selected_node.toggle_group(editor_interface, self)
	update_button_label()
	property_list_changed_notify()

func save_node_as_scene(node, path, owner):
	# https://godotengine.org/qa/903/how-to-save-a-scene-at-run-time
	var packed_scene = PackedScene.new()
	packed_scene.pack(node)
	ResourceSaver.save(path, packed_scene)
	var imported_scene = load(path)
	var my_scene = imported_scene.instance()
	selected_node.add_child(my_scene)
	my_scene.set_owner(owner)
	node.queue_free()

func handles(p_object):
	if not p_object is Node:
		return false
	var is_top_level = p_object.get_owner() == null
	var is_external = p_object.get_filename() != ""
	return p_object.get_script() == static_baker_group_const and (is_top_level or !is_external)

func make_visible(p_visible):
	if not button:
		return
	if not p_visible:
		button.hide()
		return
	if selected_node.get_script() != static_baker_group_const:
		button.hide()
		return
	update_button_label()
	button.show()

func edit(p_object):
	if(p_object == null || selected_node == p_object):
		return
	selected_node = p_object

func _init():
	print(tr("Configuring the Static Baker plugin"))

func _enter_tree():
	editor_interface = get_editor_interface()
	button = Button.new()
	button.set_button_icon(editor_interface.get_base_control().get_icon("BakedLightmap", "EditorIcons"))
	button.set_tooltip(tr("Convert collection of static meshes into a single mesh."))
	button.connect("pressed", self, "_process_static_baker_group")
	button.hide()
	add_control_to_container(CONTAINER_SPATIAL_EDITOR_MENU, button)
	add_custom_type("StaticBakerGroup", "Spatial", static_baker_group_const, editor_interface.get_base_control().get_icon("BakedLightmap", "EditorIcons"))
	add_custom_type("MaterialReplacer", "Resource", material_replacer_const, null)

func _exit_tree():
	editor_interface = null
	button.free()
	remove_custom_type("StaticBakerGroup")
	remove_custom_type("MaterialReplacer")