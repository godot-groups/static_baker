tool
extends Spatial

const material_replacer_const = preload("material_replacer.gd")
const mesh_combiner_const = preload("res://addons/mesh_combiner/mesh_combiner.gd")
const parallel_reduce_const = preload("parallel_reduce.gd")

export(Array) var original_instances = []
var material_replacer_count = 0# setget set_material_replacer_count
var material_replacers = []
var is_processing = false
export(float) var weld_distance = 0.000001
export(bool) var use_vertex_compression = false

func set_material_replacer_count(p_count):
	var initial_count = material_replacer_count
	material_replacer_count = p_count
	if material_replacer_count == initial_count:
		property_list_changed_notify()
		return
	material_replacers.resize(material_replacer_count)
	property_list_changed_notify()

func set_material_replacer(p_idx, p_material_replacer):
	if (p_idx >= material_replacers.size() || p_idx < 0):
		return
	if not p_material_replacer and p_material_replacer is material_replacer_const:
		material_replacers[p_idx] = null
		property_list_changed_notify()
		execute_material_replacers()
		return
	material_replacers[p_idx] = p_material_replacer
	property_list_changed_notify()
	execute_material_replacers()

func _get_property_list():
	var property_list = []
	property_list.push_back({"name":"material_replacers/count", "type":TYPE_INT, "hint":PROPERTY_HINT_NONE})
	for i in range(0, material_replacer_count):
		property_list.push_back({"name":"material_replacers/" + str(i) + "/material_replacer", "type": TYPE_OBJECT, "hint": PROPERTY_HINT_RESOURCE_TYPE,"hint_string":"MaterialReplacer"})
	return property_list

func _set(p_property, p_value):
	if not (p_property.begins_with("material_replacers/")):
		return
	var split_property = p_property.split("/", -1)
	if not split_property.size() > 1:
		return
	if split_property[1] == "count":
		set_material_replacer_count(p_value)
		return
	var idx = split_property[1].to_int()
	if not (idx < material_replacers.size() || idx >= 0):
		return
	if not(split_property.size() == 3 and split_property[2] == "material_replacer"):
		return
	set_material_replacer(idx, p_value)

func _get(p_property):
	if (p_property.begins_with("material_replacers/")):
		var split_property = p_property.split("/", -1)
		if split_property.size() > 1:
			if split_property[1] == "count":
				return material_replacer_count
			else:
				var idx = split_property[1].to_int()
				if (idx < material_replacers.size() || idx >= 0):
					if split_property.size() == 3 and split_property[2] == "material_replacer":
						return material_replacers[idx]

func replace_materials(p_material_replacers, p_instances):
	var mesh_instances = p_instances.mesh_instances

	for mesh_instance in mesh_instances:
		if mesh_instance is MeshInstance:
			var mesh = mesh_instance.mesh
			if mesh is ArrayMesh:
				var surface_count = mesh.get_surface_count()
				for i in range(0, surface_count):
					mesh_instance.set_surface_material(i, null)
					var mesh_material = mesh.surface_get_material(i)

					for material_replacer in p_material_replacers:
						if material_replacer != null and material_replacer is material_replacer_const:
							for material_swap in material_replacer.material_swaps:
								if mesh_material == material_swap.original_material:
									mesh_instance.set_surface_material(i, material_swap.replacement_material)
									break
			elif mesh is PrimitiveMesh:
				var mesh_material = mesh.material
				for material_replacer in p_material_replacers:
					if material_replacer != null and material_replacer is material_replacer_const:
						for material_swap in material_replacer.material_swaps:
							if mesh_material == material_swap.original_material:
								mesh_instance.set_surface_material(0, material_swap.replacement_material)
								break

func execute_material_replacers():
	replace_materials(material_replacers, process_child_instances(self, {"mesh_instances":[], "static_bodies":[]}, false, false))

func restore_backup(p_editor_interface):
	destroy_children()
	for instance in original_instances:
		var packed_scene = load(instance.path)
		var instanced_scene = packed_scene.instance(true)
		instanced_scene.set_filename(ProjectSettings.localize_path(instance.path))
		add_child(instanced_scene)
		instanced_scene.set_transform(instance.transform)
		if p_editor_interface:
			instanced_scene.set_owner(p_editor_interface.get_edited_scene_root())
	original_instances = []
	execute_material_replacers()
	property_list_changed_notify()

func backup_children(root, node):
	for N in node.get_children():
		if N.get_child_count() > 0:
			append_scene_to_dictionary(root, N)
			backup_children(root, N)
		else:
			append_scene_to_dictionary(root, N)
	property_list_changed_notify()

func append_scene_to_dictionary(root, node):
	var is_scene_file = node.get_filename() != ""
	var is_editor = Engine.is_editor_hint()
	if is_scene_file and is_editor:
		var xform = node.transform
		original_instances.append({"path": node.get_filename(), "transform": xform})

func destroy_children():
	for child in get_children():
		child.call_deferred("queue_free")
		child.get_parent().call_deferred("remove_child", child)

static func process_child_instances(p_node, p_dictionary, p_include_static_bodies, p_bake_children):
	for child in p_node.get_children():
		if child is MeshInstance and child.get_mesh() != null and not child is StaticBody:
			p_dictionary.mesh_instances.append(child)
			p_dictionary = process_child_instances(child, p_dictionary, p_include_static_bodies, p_bake_children)
			continue
		if not (child is MeshInstance and child.get_mesh() != null) and child is StaticBody:
			if p_include_static_bodies:
				p_dictionary.static_bodies.append(child)
			p_dictionary = process_child_instances(child, p_dictionary, p_include_static_bodies, p_bake_children)
			continue
		if p_bake_children == false:
			continue
		p_dictionary = process_child_instances(child, p_dictionary, p_include_static_bodies, p_bake_children)
	return p_dictionary

func toggle_group(p_editor_interface, editor_plugin):
	var undo_redo = editor_plugin.get_undo_redo()
	if original_instances.size() != 0:
		undo_redo.create_action("Unpack meshes")
		undo_redo.add_do_method(editor_plugin, "update_button_label")
		undo_redo.add_do_method(self, "restore_backup", p_editor_interface)
		undo_redo.add_undo_method(editor_plugin, "update_button_label")
		undo_redo.add_undo_method(self, "mesh_combine", p_editor_interface, editor_plugin)
		undo_redo.commit_action()
		return
	undo_redo.create_action("Pack meshes")
	undo_redo.add_do_method(editor_plugin, "update_button_label")
	undo_redo.add_do_method(self, "mesh_combine", p_editor_interface, editor_plugin)
	undo_redo.add_undo_method(editor_plugin, "update_button_label")
	undo_redo.add_undo_method(self, "restore_backup", p_editor_interface)
	undo_redo.commit_action()

func mesh_combine(p_editor_interface, editor_plugin):
	if is_processing == true:
		print("Processing")
		return
	is_processing = true
	var time_start = OS.get_unix_time()
	# https://godotengine.org/qa/3641/how-display-elapsed-time-in-game
	var state = {}
	state["editor_interface"] = p_editor_interface
	state["editor_plugin"] = editor_plugin
	state["time_start"] = time_start
	var child_instances = {}
	child_instances = process_child_instances(self, {"mesh_instances":[], "static_bodies":[]}, true, true)
	var work = []
	for mesh_instance in child_instances.mesh_instances:
		work.append({"mesh": mesh_instance.get_mesh(), "transform": get_global_transform().affine_inverse() * mesh_instance.get_global_transform()})
	state["child_instances"] = child_instances
	var reduce = parallel_reduce_const.new()
	reduce.set_show_logs(true)
	reduce.connect("notify", self, "notify")
	reduce.parallel_reduce(state, work, self, "combine", self, "handle_finished")

func handle_finished(state, work):
	var valid_instances = work
	var saved_static_bodies = []
	for static_body in state["child_instances"]["static_bodies"]:
		if not static_body is StaticBody:
			continue
		saved_static_bodies.append({"instance": static_body.duplicate(), "transform": static_body.get_global_transform()})
	state["saved_static_bodies"] = saved_static_bodies
	var combined_mesh = null
	if not use_vertex_compression:
		state["combined_mesh"] = valid_instances["mesh"]
		do_cleanup(state)
		return
	var mesh_combiner = mesh_combiner_const.new()
	mesh_combiner.append_mesh(valid_instances["mesh"], Vector2(0.0, 0.0), Vector2(1.0, 1.0), Vector2(0.0, 0.0), Vector2(1.0, 1.0), valid_instances["transform"], PoolIntArray(), weld_distance)
	combined_mesh = mesh_combiner.generate_mesh(Mesh.ARRAY_COMPRESS_DEFAULT)
	state["combined_mesh"] = valid_instances["mesh"]
	do_cleanup(state)

func combine(state_l, state_r):
	var mesh_combiner = mesh_combiner_const.new()
	if state_l != null:
		mesh_combiner.append_mesh(state_l.mesh, Vector2(0.0, 0.0), Vector2(1.0, 1.0), Vector2(0.0, 0.0), Vector2(1.0, 1.0), state_l.transform, PoolIntArray(), weld_distance)
	if state_r != null:
		mesh_combiner.append_mesh(state_r.mesh, Vector2(0.0, 0.0), Vector2(1.0, 1.0), Vector2(0.0, 0.0), Vector2(1.0, 1.0), state_r.transform, PoolIntArray(), weld_distance)
	var state = {}
	state["mesh"] = mesh_combiner.generate_mesh(0)
	state["transform"] = Transform()
	return state

func notify(state):
	state["editor_plugin"].set_button_percent_label(1 - (float(state["_work"].size()) / float(state["_work_size"])))

func do_cleanup(state):
	var elapsed =  OS.get_unix_time() - state["time_start"]
	var minutes = elapsed / 60
	var seconds = elapsed % 60
	print("\nAll instances combined. Combining time took " + "%02d:%02d" % [minutes, seconds] + "\n" )
	backup_children(self, self)
	destroy_children()
	if state["combined_mesh"] == null:
		do_static_bodies(state)
		return
	var new_mesh_instance = MeshInstance.new()
	new_mesh_instance.set_mesh(state["combined_mesh"])
	new_mesh_instance.set_name("CombinedMesh")
	add_child(new_mesh_instance)
	if not state["editor_interface"]:
		do_static_bodies(state)
		return
	new_mesh_instance.set_owner(state["editor_interface"].get_edited_scene_root())
	do_static_bodies(state)

func do_static_bodies(state):
	if state["saved_static_bodies"].size() == 0:
		state["editor_plugin"].update_button_label()
		is_processing = false
		return
	for saved_static_body in state["saved_static_bodies"]:
		var instance = saved_static_body.instance
		add_child(saved_static_body.instance)
		instance.set_global_transform(saved_static_body.transform)
		# Setup ownership
		if not state["editor_interface"]:
			continue
		instance.set_owner(state["editor_interface"].get_edited_scene_root())
		for child in instance.get_children():
			child.set_owner(state["editor_interface"].get_edited_scene_root())
	execute_material_replacers()
	state["editor_plugin"].update_button_label()
	is_processing = false

func _ready():
	if ProjectSettings.has_setting("static_baker/autobake_all") == false:
		ProjectSettings.set_setting("static_baker/autobake_all", false)
	if typeof(original_instances) != TYPE_ARRAY:
		original_instances = []
	if Engine.is_editor_hint():
		execute_material_replacers()
		return
	if original_instances.size() != 0:
		execute_material_replacers()
		return
	if ProjectSettings.get_setting("static_baker/autobake_all") != true:
		execute_material_replacers()
		return
	mesh_combine(null, null)
	execute_material_replacers()